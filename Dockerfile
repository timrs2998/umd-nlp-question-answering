FROM perl:5.14
MAINTAINER Tim Schoenheider timrs2998@gmail.com

RUN curl -L http://cpanmin.us | perl - App::cpanminus
RUN cpanm Carton

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install dependencies
COPY . /usr/src/app
RUN carton install --deployment

# Run app
CMD ["carton", "exec", "bin/app.pl"]
