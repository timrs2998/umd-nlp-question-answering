## UMD NLP Question Answering System ##

A question answering system written for CS 5761 at the University of
Minnesota Duluth under Dr. Ted Pedersen. You can follow the instructions
below to acquire the dependencies and run the code.

TODO:

The app should be runnable with Carton (see [guide](https://robn.io/docker-perl/)),
Docker, and so forth.

### Setup with plenv, cpanm, and carton ###

Install [plenv](https://github.com/tokuhirom/plenv#basic-github-checkout), its perl-build plugin, and cpanimus.

```bash
# plenv
git clone git://github.com/tokuhirom/plenv.git ~/.plenv
echo 'export PATH="$HOME/.plenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(plenv init -)"' >> ~/.bashrc
source ~/.bashrc

# plenv's perl-build plugin
git clone git://github.com/tokuhirom/Perl-Build.git ~/.plenv/plugins/perl-build/
plenv install 5.14.2
plenv rehash
plenv global 5.14.2

# cpanius
plenv install-cpanm

# carton
cpanm Carton

# run
carton install
carton exec bin/app.pl
```

Or you can use docker:

```bash
docker build -t watson .
docker run -it watson 
```

References:

 * http://kablamo.org/slides-first-cpan-module/#/
 * http://kappataumu.com/articles/modern-perl-toolchain-for-web-apps.html
 * https://engineering.semantics3.com/2016/06/15/a-perl-toolchain-for-building-micro-services-at-scale/

### Setup with system perl ###

The program is intended for and has been tested with Perl 5.14.2. You can
install all of the required dependencies like so:

    sudo su         # Might want to be root
    cpan install Data::Dumper Data::Dump WWW::Wikipedia String::Util Readonly
    exit            # Stop being root

You will also need to install git, if you haven't already, and then you can acquire the code from GitLab:

    git clone git@gitlab.com:timrs2998/umd-nlp-question-answering.git
    cd UMD-NLP-Question-Answering/

### Running ###

You simply have to run [bin/Watson.pl](bin/Watson.pl), giving it a log file, and begin asking questions. Below is an example using the QA system:

    perl bin/Watson.pl log.txt

You can ask questions like so:

    This is a QA system by Tim Schoenheider.
    It will answer Who, What, When and Where questions.
    Enter "exit" to leave the program.
    =?> [user] Who is Vladimir Putin?
    => [Dr. Watson] Vladimir Vladimirovich Putin has been the President of Russia since 7 May 2012.
    =?> [user] exit
    Thank you! Goodbye.

### Sources ###

The following were used as references and inspiration:

 * Speech and Language Processing, 2nd Edition by Daniel Jurafsky and James H. Martin. ISBN-13: 978-0131873216
 * Eric Brill, Susan Dumais, and Michele Banko. 2002. An analysis of the AskMSR question-answering system. In Proceedings of the ACL-02 conference on Empirical methods in natural language processing - Volume 10 (EMNLP '02), Vol. 10. Association for Computational Linguistics, Stroudsburg, PA, USA, 257-264. DOI=10.3115/1118693.1118726 http://dx.doi.org/10.3115/1118693.1118726
 * D.A. Ferrucci, E.W. Brown, J. Chu-Carroll, J. Fan, D. Gondek, A. Kalyanpur, A. Lally, J.W. Murdock, E. Nyberg, J.M. Prager, N. Schlaefer,  and C.A. Welty,  Building Watson: An Overview of the DeepQA Project. In Proceedings of AI Magazine. 2010, 59-79.

### Licensing ###

All code is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.
