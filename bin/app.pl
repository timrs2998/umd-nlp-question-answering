#!/usr/bin/env perl
#
# Tim Schoenheider
# CS 5761
# Ted Pedersen
# 12-12-2013
# Assignment 6
#

=pod

################################################################################
1) Problem
The problem is to implement a question answering (QQ) system in perl inspired by
AskMSR and Watson. Given a question, the system is to search wikipedia for the
answer and return a complete sentence. The sentence must be an admission of
failure or the answer.

################################################################################
2) Input, output, and usage
The script, qa-system.pl, can be executed as a standard perl script with one
argument. The argument is the file where questions, search queries, results, the
answers, and their confidence scores are logged. An example is:
	perl qa-system.pl log.txt

The script prompts the user for input and the user can input questions. Input
must begin with who, what, when, or where and must end with a question mark. An
Example is:
	This is a QA system by Tim Schoenheider.\n";
	It will answer Who, What, When and Where questions.
	Enter "exit" to leave the program.
	=?> [user] When was George Washington born?

Then the system will log the appropriate information and print the answer to
stdout. An answer may appear as:
	=> [Dr. Watson] George Washington was born on February 11, 1731.

After answering or not answering a question, the user will be prompted for input
again.

In the log file, every question causes a block similar to the following to be
written:
	Question: When was George Washington born?
	Found 'George' article on Wikipedia with query 'George'
	Found 'George%20Washington' article on Wikipedia with query 'George
		Washington'
	Found 'born' article on Wikipedia with query 'born'
	Found 'Washington' article on Wikipedia with query 'Washington'
	Answer: George Washington was born on February 11, 1731.

For every question asked, the question is written, the wikipedia articles and
the query that found it are written, and the final answer that was given to the
user are  written in the above format.

################################################################################
3) Algorithm (same as assignment 5 except for sections at the end)
The code is separated into several modules and this script. The qa-system.pl
file is this script. The NGram.pm file is a modularized version of
assignment #2, which allows easily creating ngrams from a given text. The
Tagger.pm is an unused, modularized version of assignment #3 (a POS tagger) that
relies on pos-train.txt to train itself. Lastly, Watson.pm is the qa system in a
modularized form.

This script simply creates a Watson.pm object and enters an input loop where it
passes user input to the Watson module and prints its answer.

The Watson.pm has an ask() subroutine that accepts a question passed in from
this script. The ask() method goes through the three stages of query processing,
information retrieval, and answer processing.

The query given to ask() is passed to reformulateQuery() which creates queries
from relevant unigrams, bigrams, and trigrams found in the original query. Then,
the original query is passed to getTemplates() which creates a list of answer
'templates' by moving the form of 'to be' found in the question around, as well
as moving a placeholder _ around. These permutations are used later to fit a
retrieved passage into an answer template. Depending on the question word,
permutations may involve new words such as 'on' or 'in'.

In the second stage, the reformulated queries are passed to retrieveDocuments().
This subroutine looks for wikipedia articles whose title matches the queries,
and follows related links for some queries (with a depth of 1). The documents
are stripped of parethetical and bracketed information, as well as cleaned up.
Parts of the document are fed into an trigram which can be used (but currently
isn't) to rank potential answers based on how likely they are according to the
language model represented by the trigram. Each document is passed to
retrievePassages() along with the original question. A sentence (passage) is
extracted from the document if it has 65% of the words found in the original
question.

In the third stage, each passage is checked to see if it matches a template.
There can be a match in two ways: the passage matches the template and can fill
in the '_' placeholder, or the passage contains all of the words of the template
in the same order. The second way of matching is more effective. A matched
passage is used to fill the corresponding template to make a potential answer.
Potential answers chosen based on their question word. In most cases, the first
potential answer is accepted as the final answer. With dates, however, the first
potential answer to contain a year or month is accepted. This works because the
first answer corresponds to the first passage from the first document, which is
typically a wikipedia summary containing the exact answer to the question. In
that way, answers are yet to be truly compared and ranked. Once an answer is
chosen, a subject is chosen from the question to be either a series of
capitalized nouns or everything after the form of 'to be' that appears. This
subject is used to replace everything before the form of 'to be' found in the
potential answer. This corrects responses such as "Washinton was born on ..." to
"George Washington was born on ..." and more. The ask() method then returns the
answer or returns an admission of failure. In qa-system.pl, the system prompts
for user input once again.


In assignment 6, however, the following additions to this algorithm are made:


################################################################################
# Confidence Scoring of Answers ################################################
################################################################################
The confidence score is based solely off of the final potential answers. It
consists of three parts: how well it conforms to a language model, the order
in which it was found, and how many words of the original question it contains.
Each part is a third of the scoring (out of 100%). The first part is essentially
the probability of the sentence occurring in English using a bigram model, where
the bigram consists of all wikipedia articles ever read by the QA system. The
second part is 1 - (order found / # of potential answers). In that way, answers
found first are favored, since passages that occur late in Wikipedia articles
are less likely to be the correct answer. Lastly, the number of words from the
original question found in the candidate answer is divided by the total number
of words in the question (ignoring extremely common words). After a score is
calculated, the score may be penalized for not ending in a period or for not
having a month or year for a 'when' question; 33.33% of the score is then
subtracted away. The answer with the highest score is chosen, provided that its
confidence is greater than 30%. A lower confidence is regarded as a failure.

################################################################################
# Enhancement 1 to Query Reformulation
################################################################################
Depending on the question type, words are appended to the end of the query. For
example, a 'when' question appends the words on, in, and during and potential
answers must have those words. Similarly, a 'where' query must end in at, in,
on, or located. Adding these words which do not appear in the query helps narrow
down the list of potential answers.

################################################################################
# Enhancement 2 to Query Reformulation #########################################
################################################################################
A list of terms is build by considering every synonym of each word in the
original query. These synonyms are pulled from a website interface to WordNet
using the WordNetSynset.pm module. The list of terms is used to find additional
passages that may contain answers, and is used to create more queries. New
templates are generated by replacing a word with each of its synonyms, for each
word in the template. Adding these synonyms allows the QA system to match
'killed' or 'murdered' when the original query used 'shot'.

################################################################################
# Enhancement 1 to Answer Composition ##########################################
################################################################################
The QA system matches sentences from templates, but a template only has to be
matched by a passage having all of the same words in the same order as the
template -- there can be intervening words and the placeholder is ignored. In
that way, an answer can be more precise or verbose than a question calls for,
broadening the range of possible answers.


################################################################################
# Enhancement 2 to Answer Composition ##########################################
################################################################################
The QA system takes the most highly scored answer, and the second most highly
scored answer and tries to merge them. First, it converts the second to a hash
of bigrams, trying to complete the first answer with the second answer's
bigrams. Then, the QA system treats the first answer as a regular expression
with spaces converted to .*?. If the second answer matches this regex, the
second answer is reaadded to the list as the new first answer.

As a result, the second answer can either be lengthier, more exact first answer
or it can be tiled onto the first answer if bigrams overlap with the end of the
first answer.



=cut

package QASystem;

use strict;
use warnings;
use Readonly;
use Data::Dump qw(dump);
use Carp;
use WWW::Wikipedia;

use lib 'lib/';     # TODO: use FindBin
use Watson;
use Tagger;

Readonly our $VERSION => '1.00';
my $LOG_FILENAME = $ARGV[0];


################################################################################
# Main code ####################################################################
################################################################################

sub main {
	# Print introductory message
	print "This is a QA system by Tim Schoenheider.\n";
	print "It will answer Who, What, When and Where questions.\n";
	print "Enter \"exit\" to leave the program.\n";

	my $watson = Watson->new(
		language => 'en',
		log_file => $LOG_FILENAME
	);

	# successful questions
	# output($watson->ask('When was George Washington born?'));
	# output($watson->ask('When was Vincent Brady born?'));
	# output($watson->ask('What is a bicycle?'));
	# output($watson->ask('What was the Soviet Union?'));
	# output($watson->ask('What is the PRISM program?'));
	# output($watson->ask('Where is Balato?'));
	# output($watson->ask('Who is Vladimir Putin?'));
	# output($watson->ask('Who is Dick Cheney?'));
	# output($watson->ask('Who are the Sioux?'));

	# failure questions
	# output($watson->ask('Where is Korea?'));
	# output($watson->ask('What is the Brussels Agreement?'));
	# output($watson->ask('Who was Lincoln\'s murderer?'));
	# output($watson->ask('When was Mohandas Gandhi assassinated?'));
	# output($watson->ask('When was Ronald Reagan shot?'));
	# output($watson->ask('When was the first calculator created?'));
	# output($watson->ask('What is the temperature in Paris today?'));
	# output($watson->ask('What is wave velocity?'));
	# output($watson->ask('Where was Barack Obama on February 22, 2010?'));
	# output($watson->ask('Where is Duluth, Minnesota?'));
	# output($watson->ask('Where does Glenn Greenwald live?'));
	# output($watson->ask('Who assassinated Abraham Lincoln?'));
	# output($watson->ask('Who is the coach of the Minnesota Vikings?'));

	# Begin asking for input
	for (my $line = input(); $line !~ /\bexit\b/i; $line = input()) {
		# Get answer from Dr. Watson, print it
		my $answer = $watson->ask($line);
		output($answer);
	}

	print "Thank you! Goodbye.\n";
}

################################################################################
# Code that deals with I/O #####################################################
################################################################################

# Prompts user for input, returns the input.
sub input {
	print "=?> [user] ";

	# Get input from the user
	my $input = <STDIN>;

	# Clean input
	$input =~ s/\b(who's)\b/who is/i;
	$input =~ s/\b(what's)\b/what is/i;
	$input =~ s/\b(where's)\b/where is/i;

	# Return the user's modified input
	return $input;
}

# Print output to user, format it nicely
sub output {
	my $output = $_[0];

	# Remove trailing whitespace, return the final output
	$output =~ s/^(.*)\s+$/$1/;
	print "=> [Dr. Watson] $output\n";
}

################################################################################
# Call main() method, begin execution. Done last to ensure initialization of
# everything else.
################################################################################

main();
