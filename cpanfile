requires 'perl', '5.14.2';

requires 'Data::Dumper', '2.154';
requires 'Data::Dump', '1.23';
requires 'WWW::Wikipedia', '2.04';
requires 'String::Util', '1.24';
requires 'Readonly', '2.00';
