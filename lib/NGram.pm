package NGram;

use strict;
use warnings;
use Readonly;
use Carp;
use String::Util 'trim';
use List::Util 'max';
use List::Util 'sum';
use utf8;

Readonly our $START_TAG      =>      '<s>';
Readonly our $STOP_WORD 		=> 		qr/( \. | \? | \! )/xmsi;
Readonly our $COUNT 			=> 		'_count_ im a unique string';

sub new {
	my $invocant = shift;
	my $class = ref($invocant) || $invocant;
	my $self = {
		n => 3,
		hash => {},
		case_sensitive => 1,
		@_,
	};
	return bless($self, $class);
}

sub n {
	my ($self) = @_;
	return $self->{n};
}

sub case_sensitive {
	my ($self) = @_;
	return $self->{case_sensitive};
}

sub hash {
	my ($self) = @_;
	return $self->{hash};
}

sub addText {
	my ($self, $text) = @_;	
	my $n = $self->n();	

	# Initialize array of last n - 1 words as START_TAG
	my @prev = $self->createPrev();

	# Iterate through all words and end of sentence markers
	while ($text =~ /([a-z'0-9]+|"|:|,|;|-|\.|\?|\!)/ig) {
	    my $word = $self->case_sensitive ? $1 : lc($1);
	    
		# Let $phrase be the previous n - 1 words
		my $phrase = join(' ', @prev);

		# Increment count for $word given $phrase
		$self->hash->{$phrase}{$word} += 1;
		
		# Increment count for the phrase
		$self->hash->{$phrase}{$COUNT} += 1;

	    # Reset @prev if current word is a STOP_WORD, otherwise update
	    # the list of previous n - 1 words
	    if ($word =~ $STOP_WORD) {
	        @prev = $self->createPrev();
	    } elsif ($n > 1) {
	        shift @prev;
	        push @prev, $word;
	    }
	}
	return;
}

# Creates and returns an array containing n - 1 START_TAGs
sub createPrev {
	my ($self) = @_;	
	my $n = $self->n();	

    my @array = ();
    for (0 .. $n - 2) {
        push @array, $START_TAG;
    }
    return @array;
}

################################################################################
# Code for ngram mining
################################################################################

# Mines a given ngram for a given phrase. Returns the most likely complete
# sentence
sub tileNgram {
	my ($self, $phrase) = @_;	
	my $n = $self->n();
	my %ngram = %{$self->hash()};


	# Expand left (until START reached)
	
	# Expand right (until end-of-sentence marker reached)
	my $sentence = $self->expandRight($phrase);

	# Return expanded sentence
	return $sentence;
}

# Generates words separated by a space until reaching END_TAG
sub expandRight {
	my ($self, $sentence) = @_;

	my $n = $self->n();
	my %ngram = %{$self->hash()};
    my @prev = $self->createPrev();

    # Create words until STOP_WORD is found
    my $word;
    do {
        # Using the last n - 1 words, append a new word to the sentence
        $word = getWord($n, \%ngram, rand(), @prev);
        $sentence .= " ".$word;
        
        # Keep track of last n - 1 words if this isn't the unigram model
        if ($n > 1) {
            shift @prev;
            push @prev, $word;
        }
    } while ($word !~ $STOP_WORD);
    
    # Strip whitespace from the beginning of the sentence and return it
    return trim($sentence);
}

# Given a probability p such that 0 <= p <= 1, and n - 1 words, returns a word
# such that p = p(word | phrase).
sub getWord {
	my $n = $_[0];
	my %ngram = %{$_[1]};

    # Read in the parameters, with $probability being the first and 
    # $phrase being the n - 1 words or empty string for the unigram model.
    my $probability = $_[2];
    my $phrase = $n == 1 ? '' : join(' ', @_[3 .. $n + 1]);

    # TODO: ARG MAX, not max
    # my $word = max map { $_ eq $COUNT ? 0 : $ngram{$phrase}{$_} } (keys %{$ngram{$phrase}});

    # Iterates through each probability until $curProb exceeds $probability to
    # find the word corresponding to the given probability.
    my $curProb = 0;
    foreach my $word (keys %{$ngram{$phrase}}) {
    	next if $word eq $COUNT;

        # Let $count_phrase_word be C(w1 w2 .. wn-1 wn)
        my $count_phrase_word = $ngram{$phrase}{$word};
        
        # Let $count_phrase be C(w1 w2 .. wn-1)
        my $count_phrase = $ngram{$phrase}{$COUNT};
        
        # Append probability of p(wn | w1 w2 .. wn-1) to $curProb
        $curProb += $count_phrase_word / $count_phrase;
        
        # If the $curProb exceeds the $probability, then return current word
        if ($probability <= $curProb) {
            return $word;
        }
    }
    croak 'Error occurred';
}

sub getProbability {
	my ($self, $text) = @_;	
	my $n = $self->n();	
	no warnings 'uninitialized';

	my $probability = 1;
	my $zero = 0.01;

	# Initialize array of last n - 1 words as START_TAG
	my @prev = $self->createPrev();

	# Iterate through all words and end of sentence markers
	while ($text =~ /([a-z'0-9]+|"|:|,|;|-|\.|\?|\!)/ig) {
	    my $word = $self->case_sensitive ? $1 : lc($1);
	    
		# Let $phrase be the previous n - 1 words
		my $phrase = join(' ', @prev);

		# Get the probability of these n words occuring
		my $num = $self->hash->{$phrase}{$word};
		my $den = $self->hash->{$phrase}{$COUNT};
		my $phrase_probability = $den == 0 ? 0 : $num / $den;
		($phrase_probability = $zero) if $phrase_probability == 0;

		# Maintain running probability
		$probability *= $phrase_probability;

	    # Reset @prev if current word is a STOP_WORD, otherwise update
	    # the list of previous n - 1 words
	    if ($word =~ $STOP_WORD) {
	        @prev = $self->createPrev();
	    } elsif ($n > 1) {
	        shift @prev;
	        push @prev, $word;
	    }
	}
	return $probability;
}

# Needed because it's a module
1;