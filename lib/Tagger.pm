package Tagger;
use strict;
use warnings;

use List::Util 'max';
use List::Util qw(first);
use Data::Dump qw(dump);
use List::Util qw(sum);
use String::Util 'trim';

use constant START_TAG => '<s>';
use constant STOP_REGEX => qr{
    /(\.|\?|\!)/
}xmsi;

sub new {
    my $invocant = shift;
    my $class = ref($invocant) || $invocant;
    my $self = {
        bigram => {},
        unigram => {},
        word_tag => {},
        default => 'NN',
        case_of_first_matters => 1,
        @_,
    };
    return bless($self, $class);
}

sub tag {
    my ($self, $text) = @_;

    # Trim whitespace, space punctuation separate from word
    $text = trim($text);
    $text =~ s/(.*)(\.|\?|\!|\,)/$1 $2/g;

    my $tagged_text = '';
    my $count = 0;
    my @words = ();
    my @words_cased = ();

    # Iterate through text to be tagged
    while ($text =~ /([^\[\]\s]+)/g) {
        my $word = lc($1);
        push @words, $word;
        push @words_cased, $1;

        # If we're at the end of the sentence, find and print the tags
        # to stdout
        if ($word =~ STOP_REGEX) {
            # Get the best tags for the current sentence
            # my @tagset = getBigramTagset(\@words);
            my @tagset = $self->getUnigramTagset(\@words);

            # Appy manual rules
            @tagset = $self->applyRules(\@words_cased, \@tagset);

            # Print the words and their tags to stdout
            $tagged_text .= $self->stringifyWordsAndTags(\@words_cased, \@tagset).' ';
            @words = ();
        }
    }

    # In case last word wasn't a stop word, find tags for the last time
    if (@words) {
        # my @tagset = getBigramTagset(\@words);
        my @tagset = $self->getUnigramTagset(\@words);
        @tagset = $self->applyRules(\@words_cased, \@tagset);
        
        # Print the words and their tags to stdout
        $tagged_text .=  $self->stringifyWordsAndTags(\@words_cased, \@tagset).' ';
    }
    return $tagged_text;
}

sub train {
    my ($self, $training) = @_;
    
    # Previous word
    my $prev = START_TAG;
    
    # Iterate through word/tag pairs, construct bigram and unigram
    # models
    while ($training =~ /(\S+)\/(\S+)(\|\S*)?/ig) {
        my $word = lc($1);
        my $tag = $2;
        
        # Build unigram and bigram models of the training data
        $self->bigram()->{$prev}{$tag} += 1;
        $self->unigram()->{$tag} += 1;
        $self->word_tag()->{$word}{$tag} += 1;
        
        # $prev is START_TAG is we're at the end of sentence, $word otherwise
        $prev = ($word =~ STOP_REGEX) ? START_TAG : $word;
    }
    return;
}

# Getters
sub bigram {
    my ($self, $bigram) = @_;
    return $self->{bigram};
}
sub unigram {
    my ($self, $unigram) = @_;
    return $self->{unigram};
}
sub word_tag {
    my ($self, $word_tag) = @_;
    return $self->{word_tag};
}
sub default {
    my ($self, $default) = @_;
    return $self->{default};
}
sub case_of_first_matters {
    my ($self, $case_of_first_matters) = @_;
    return $self->{case_of_first_matters};
}

# Given a reference to an array of words and a reference to an array of tags,
# creates a string representation of the word/tag pairs and returns it.
sub stringifyWordsAndTags {
    my ($self, $words_ref, $tags_ref) = @_;
    my @words = @{$words_ref};
    my @tags = @{$tags_ref};

    # Concatenate words and tags into a string
    my $output = "";
    for my $i (0 .. @words - 1) {
        $output .= $words[$i]."/".$tags[$i]." ";
    }
    return $output;
}

# Given a reference to an array of words and a reference to an array of tags,
# applies various rules and returns the modified tags.
sub applyRules {
    my ($self, $words_ref, $tags_ref) = @_;
    my @words = @{$words_ref};
    my @tags = @{$tags_ref};

    my $prev_word = START_TAG;
    my $prev_tag = START_TAG;
    for my $i (0 .. @words - 1) {
        # Rules can be added here:

        # 1) Words with capital letter are NNP
            # If the case of the first word matters, then count it
            # otherwise count it as normal
        my $case_of_first_matters = $self->case_of_first_matters();
        if ($words[$i] =~ /^[A-Z][a-z]+/ && ($i != 0 || !$case_of_first_matters)) {
            $tags[$i] = 'NNP';
        }

        # Update previous word and tag
        $prev_word = $words[$i];
        $prev_tag = $tags[$i];
    }
    return @tags;
}

###########################################################
# Code for unigram POS tagger begins here #################
###########################################################

# Given a reference to an array of words, returns an array of tags
# for those words using a unigram POS tagger.
sub getUnigramTagset {
    my ($self, $words_ref) = @_;
    my @words = @{$words_ref};

    my @tags;
    foreach my $word (@words) {
        push @tags, $self->getUnigramTag($word);
    }
    return @tags;
}

# Given a word, returns its tag using a unigram POS tagger
sub getUnigramTag {
    my ($self, $word) = @_;

    # If word was never seen then let the tag be NN
    unless (exists $self->word_tag()->{$word}) {
        return $self->default();
    }

    # Otherwise, find the tag, ti, that maximizes P(ti | w)
    my %counts = %{$self->word_tag()->{$word}};
    my $max_tag = (keys %counts)[0];
    foreach my $tag (keys %counts) {
        # Instead of comparing P(t | w), we compare C(t | w)
        if ($counts{$tag} > $counts{$max_tag}) {
            $max_tag = $tag;
        }
    }
    return $max_tag;
}


############################################################
############################################################
# UNUSED CODE FOR BIGRAM POS TAGGER ########################
# (bugs: infinite loop for some sentences, otherwise works)#
# You can stop reading tagger.pl here. #####################
############################################################

sub getBigramTagset {
    my ($self, $words_ref) = @_;

    my @words = @{$words_ref};
    my @tagsets = $self->getTagsets(\@words);

    # Use equation to find most likely tagset
    my $index = 0;
    my $max = $self->getTagsetProbability(\@words, $tagsets[0]);
    foreach my $i (0 .. @tagsets - 1) {
        my $probability = $self->getTagsetProbability(\@words, $tagsets[$i]);
        if ($probability > $max) {
            $index = $i;
            $max = $probability;
        }
    }
    return split(' ', $tagsets[$index]);
}

# Given a list of words, returns an array of strings where each string
# is one possible tagset. The array returned has all possible tagsets.
sub getTagsets {
    my ($self, $words_ref) = @_;

    my @words = @{$words_ref};
    my $count = 0;

    # Array of all possible tagsets represented by strings
    my @tagsets = (); 

    foreach my $word (@words) {
        # Copy old tagsets to permute them, destroy original
        my @copy = @tagsets;
        @tagsets = ();

        # Permute tagsets
        foreach my $tag (keys %{$self->word_tag()->{$word}}) {
            foreach my $tagset (@copy) {
                push @tagsets, $tagset." ".$tag;
            }

            if (@copy == 0) {
                push @tagsets, $tag;
            }
        }

        if (keys %{$self->word_tag->(){$word}} == 0) {
            foreach my $tagset (@copy) {
                push @tagsets, $tagset." NN";
            }

            if (@copy == 0) {
                push @tagsets, "NN";
            }
        }

        # die "Max exceeded" if $count > MAX_COUNT;
        $count += 1;
    }
    return @tagsets;
}

sub getTagsetProbability {
    my ($self, $words_ref, $tags) = @_;

    my @words = @{$words_ref};
    my @tagset = split(' ', $tags);

    my $probability = 1;
    for my $i (0 .. @words) {
        my $prev_tag = $i == 0 ? START_TAG : $tagset[$i - 1];
        my $tag = $tagset[$i];
        my $word = $words[$i];

        my $tag_transition = $self->getTagTransitionProbability($prev_tag, $tag);
        my $word_likelihood = $self->getWordLikelihoodProbability($tag, $word);

        $probability *= $tag_transition * $word_likelihood;
    }
    return $probability;
}

sub getTagTransitionProbability {
    my ($self, $prev_tag, $tag) = @_;
    no warnings 'uninitialized';

    my $num = $self->bigram()->{$prev_tag}{$tag};
    my $den = sum values %{$self->bigram()->{$prev_tag}};
    return $den == 0 ? 0 : $num / $den;
}

sub getWordLikelihoodProbability {
    my ($self, $tag, $word) = @_;
    no warnings 'uninitialized';

    my $num = $self->word_tag()->{$word}{$tag};
    my $den = sum values %{$self->word_tag()->{$word}};
    return $den == 0 ? 0 : $num / $den;
}

1;