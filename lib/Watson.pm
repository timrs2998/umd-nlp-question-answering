package Watson;

use strict;
use warnings;
use Readonly;
use WWW::Wikipedia;
# use Data::Dump qw(dump);
use String::Util 'trim';
use Carp;
use Class::Struct;
use utf8;

use NGram;
use Tagger;
use WordNetSynset;

# Hash reference used to abstract language-specific words
Readonly my $structures => {
	en => {
		who => 'who',
		what => 'what',
		when => 'when',
		where => 'where',
		why => 'why',
		how => 'how',
		be => [ 'am', 'is', 'are', 'was', 'were', 'have been', 'am being',
			'are being', 'was being', 'were being',	'had been being',
			'have been', 'has been', 'will have been being',
			'will be', 'will be being', 'will have been',
			'will have been being'
		],
		no_answer => 'I could find an answer to the question.',
	},
	es => {
		who => 'quién',
		what => 'qué',
		when => 'cuándo',
		where => 'dónde',
		why => 'por qué',
		how => 'cómo',
		be => [ 'es', 'está' ],
		no_answer => 'No pude encontrar una respuesta a la pregunta.',
	},
};

# Regex that matches against extremely common words
Readonly my $COMMON => qr {
	^(the|is|are|be|a|an|of|to|and|who|what|when|where|\?)$
}xmsi;

################################################################################
# Constructor, getters, and setters adapted from WWW:Wikipedia source on CPAN
################################################################################
sub new {
	my $invocant = shift;
	my $class = ref($invocant) || $invocant;
	my $self = {
		language => 'en',
		tagger => Tagger->new(
			default => 'UNKNOWN',
			case_of_first_matters => 0
		),
		grammar_gram => NGram->new (
			n => 2
		),
		log_file => 'log.txt',
		@_,
	};
	return bless($self, $class);
}
sub language {
	my ($self, $language) = @_;
    $self->{language} = $language if $language;
	return $self->{language};
}
sub tagger {
	my ($self, $tagger) = @_;
    $self->{tagger} = $tagger if $tagger;
	return $self->{tagger};
}
sub grammar_gram {
	my ($self, $grammar_gram) = @_;
	return $self->{grammar_gram};
}
sub log_file {
	my ($self, $log_file) = @_;
	return $self->{log_file};
}


################################################################################
# Main entry point into Watson.pm. Given a question, returns an answer.
################################################################################
sub ask {
	my ($self, $question) = @_;
	$self->log('Question: '.trim($question));

	# 1) Query processing
	# 	- Query reformulation

	# Feature 1 - finding synonyms
	my @terms = ();
	foreach my $word (split /[\s\?]/, $question) {
		if ($word =~ /$COMMON/) { next; }
		push @terms, $word;
		if ($word =~ /^[A-Z][a-z]+$/) { next; }

		my @synset = WordNetSynset::getSynset($word);
		foreach my $synonym (@synset) {
			push @terms, $synonym;
		}
	}
	@terms = removeDuplicates(@terms);

	my @queries = $self->reformulateQuery($question);

	#	- Query classification / answer patterns
	my @templates = $self->getTemplates($question);


	# 2) Information retrieval
	#	- Document retrieval
	my @documents = $self->retrieveDocuments(1, \@queries);
	#	- Passage retrieval
	my @passages = ();
	foreach my $document (@documents) {
		push @passages, retrievePassages($document, $question, @terms);
	}


	# 3) Answer processing
	# 	- Extract answers from the passages
	my @answers = ();
	foreach my $passage (@passages) {
		foreach my $template (@templates) {
			# Create an answer from an exact template match against the passage
			my $answer = $self->rewordAnswer(
				$question,
				extractAnswer($passage, $template)
			);
			push(@answers, $answer) if $answer;

			# OR match up the words in the template to the words in the passage
			# to create an answer
			unless ($answer) {
				$answer = $self->rewordAnswer(
					$question,
					extractLooseAnswer($passage, $template)
				);
				push(@answers, $answer) if $answer;
			}
		}
	}

	# Tile answers together
	@answers = removeDuplicates(@answers);
	my @tiled = $self->tileAnswers($question, @answers);
	unshift @answers, @tiled;


	# Pick an answer (typically first one)
	my $answer = $self->pickAnswer($question, @answers);

	# Log the answer and a newline
	if ($answer) {
		$self->log('Answer: ' . $answer);
	} else {
		$self->log('Answer:'
			. $structures->{$self->language()}->{no_answer});
	}
	$self->log('');

	# General debugging:
	# dump @templates;
	# dump @passages;
	# dump @answers;

	# Return either an answer or a statement of failure
	return defined $answer ? $answer
		: $structures->{$self->language()}->{no_answer};
}

# Given a string, logs it to $self->log_file()
sub log {
	my ($self, $string) = @_;
	open(my $file, q{>>}, $self->log_file());
	print $file "$string\n";
	close($file);
	return;
}




################################################################################
# 1) Code for query processing #################################################
################################################################################

# Given a question, creates and returns a list of search queries
sub reformulateQuery {
	my ($self, $question) = @_;
	my @queries = ();

	# Match against parts of question we want to look for
	my $question_word = lc($self->getQuestionWordOrUndef($question));
	my $phrase_regex = qr { ^ [¿]? $question_word (.*)\? $}xmsi;

	if ($question =~ $phrase_regex) {
		my $phrase = trim($1);

		# Add all individual words as queries
		push @queries, split(' ', $phrase);

		# Create ngrams of query
		my $two_gram = NGram->new ( n => 2 );
		my $three_gram = NGram->new( n => 3 );
		$two_gram->addText($phrase);
		$three_gram->addText($phrase);

		# Add all pairs/triples of words as queries
		my $count = $NGram::COUNT;
		my $start = $NGram::START_TAG;
		foreach my $single (keys $two_gram->hash()) {
			foreach my $second (keys $two_gram->hash()->{$single}) {
				unless ($count eq $second) {
					push @queries, $single.' '.$second;
				}
			}
		}
		foreach my $pair (keys $three_gram->hash()) {
			foreach my $third (keys $three_gram->hash()->{$pair}) {
				unless ($count eq $third) {
					push @queries, $pair.' '.$third;
				}
			}
		}

		# Remove silly queries
		@queries = grep { !/\b(I|is|was|a|an|the|un|una|on|in|en|of)\b/i }
			@queries;

		# Remove whitespace before commas
		my @queries_copy = @queries;
		@queries = ();
		foreach my $query (@queries_copy) {
			$query =~ s/\s\,/\,/g;
			push @queries, $query;
		}

		# Duplicate queries that have commas as queries without commas
		@queries_copy = @queries;
		foreach my $query (@queries_copy) {
			if ($query =~ s/\,//g) {
				$query =~ s/\s+/ /g;
				push @queries, $query;
			}
		}

		# Remove duplicate queries
		@queries = keys %{{ map { $_ => 1 } @queries }};
	}
	return removeDuplicates(@queries);
}

# Given a question, returns the question word (who, what, when, or where).
sub getQuestionWordOrUndef {
	my ($self, $question) = @_;
	my $structs = $structures->{$self->language()};

	# Check against all the question words in the language structure
	my $question_word = undef;
	foreach my $candidate ($structs->{who}, $structs->{what}, $structs->{when},
			$structs->{where}) {
		my $question_word_regex = qr {
			^[¿]? ($candidate) .* \? $
		}xmsi;

		# If the first word matches who, what, when, or where for this language,
		# return it
		if ($question =~ /$question_word_regex/) {
			$question_word = $1;
		}
	}
	return $question_word;
}

# Given a statement starting with a form of 'to be', returns a list of
# permutations of the statement by moving 'to be' throughout it, as well as
# moving a placeholder '_' between all words after 'to be'. Also creates
# permutations that have everything before 'to be' stripped. This method is
# used to create answer templates by getTemplates()
sub permuteBe {
	my ($self, $question) = @_;

	my @permutations = ();
	my $structs = $structures->{$self->language()};

	# Foreach form of the word 'to be' ...
	foreach my $be (@{$structs->{be}}) {
		# Regex that matches any question with 'to be'
		my $q1 = qr{ $be \s (.*?) \? }xmsi;

		# If it matches the regular expression ...
		if ($question =~ /$q1/) {
			# Permute the question for every word
			my @word_array = split ' ', $1;

			foreach my $i (0 .. $#word_array) {
				# Insert $be _ after every word
				my $string = '';
				foreach my $j (0 .. $i) {
					$string .= $word_array[$j];
					$string .= ' ';
				}
				$string .= "$be _ ";
				foreach my $j ($i + 1 .. $#word_array) {
					$string .= $word_array[$j];
					$string .= ' ';
				}
				push @permutations, trim($string);

				# Insert $be after every word, put _ at the end
				$string = '';
				foreach my $j (0 .. $i) {
					$string .= $word_array[$j];
					$string .= ' ';
				}
				$string .= "$be ";
				foreach my $j ($i + 1 .. $#word_array) {
					$string .= $word_array[$j];
					$string .= ' ';
				}
				push @permutations, trim($string.'_')
					unless $i == $#word_array;
			}

			# Add permutations removing all words before be
			my @permutations_copy = @permutations;
			foreach my $permutation (@permutations_copy) {
				my $regex = qr { \b $be \b (.*?) $	}xmsi;
			 	if ($permutation =~ $regex) {
			 		my $right = trim($1);
			 		push(@permutations, $be.' '.$right)
			 			unless length($right) < 4;
			 	}
			}
		}
	}
	return @permutations;
}

# Given a question, attempts to return all possible templates that an answer
# might match.
sub getTemplates {
	my ($self, $question) = @_;
	my $structs = $structures->{$self->language()};

	# Get the question word
	my $question_word = lc($self->getQuestionWordOrUndef($question));

	# Create answer templates based on the question word
	my @templates = ();

	if ($structs->{who} eq $question_word) {
		my @permutations = $self->permuteBe($question);
		push @templates, @permutations;
		# note that _ should favor people (proper nouns)

	} elsif ($structs->{what} eq $question_word) {
		my @permutations = $self->permuteBe($question);
		push @templates, @permutations;
	} elsif ($structs->{when} eq $question_word) {
		my @permutations = $self->permuteBe($question);

		# note that _ should favor dates (on)
		foreach my $permutation (@permutations) {
			push @templates, ($permutation =~ s/_/on _/r); #/
			push @templates, ($permutation =~ s/_/in _/r); #/
			push @templates, ($permutation =~ s/_/during _/r); #/
		}

	} elsif ($structs->{where} eq $question_word) {
		my @permutations = $self->permuteBe($question);

		foreach my $permutation (@permutations) {
			push @templates, ($permutation =~ s/_/at _/r); #/
			push @templates, ($permutation =~ s/_/in _/r); #/
			push @templates, ($permutation =~ s/_/on _/r); #/
			push @templates, ($permutation =~ s/_/located _/r); #/
		}
		# note that _ should favor locations (proper nouns)
	}

	# Create new templates from synonyms
	my @syn_templates = ();
	foreach my $template (@templates) {
		my @words = split ' ', $template;
		foreach my $word (@words) {
			my @synset = WordNetSynset::getSynset($word);
			foreach my $synonym (@synset) {
				push @syn_templates, $template =~ s/\b$word\b/$synonym/r; #/
			}
		}
	}
	push @templates, @syn_templates;
	return removeDuplicates(@templates);
}

################################################################################
# 2) Code for information retrieval ############################################
################################################################################

# Given text from wikipedia, cleans it by removing reference tags, parenthetical
# and bracketed text, quotes, pipes, and so on. Returns the clean copy.
sub cleanText {
	my ($self, $text) = @_;

	# Remove reference tags
	my $ref_regex = qr {
		( <ref .*? /?> ( .*? </ref> )? )
	}xmsi;
	$text =~ s/$ref_regex//g;

	# Convert pipes and ]] to spaces
	$text =~ s/(\||\]\])/ /g;

	# Remove quotes
	$text =~ s/(\"|\')//g;

	# Remove information in parenthesis, brackets, and so on
	foreach my $pair ('()', '[]', '{}') {
		my $left = (split '', $pair)[0];
		my $right = (split '', $pair)[1];

		my $pair_regex = qr {
			( \Q$left\E .*? \Q$right\E )
		}xmsi;
		$text =~ s/$pair_regex//g;
	}

	# Convert whitespace to spaces
	$text =~ s/\s+/ /g;

	return $text;
}

# Parameters are a boolean and a reference to an array of queries. The boolean
# determines whether to follow related links or not. The list of queries is used
# to find wikipedia pages of the same title, clean them, and returnthem. Related
# links are only followed with a depth of 1 for multiword queries. Also learns
# a language model based on summaries of the articles.
sub retrieveDocuments {
	my ($self, $follow, $queries_ref) = @_;
	my @queries = @{$queries_ref};

	my $wiki = WWW::Wikipedia->new( language => $self->language() );
	my @documents = ();
	foreach my $query (@queries) {
		# Search for wikipedia page, skip query if it doesn't exist
		my $result = $wiki->search($query);
		next unless defined $result;

		# Log the wikipedia page and the query
		$self->log('Found \''
			. $result->title()
			. "' article on Wikipedia with query '$query'");

		# Follow related links once on disambiguation pages for multi-word
		# queries
		if ($follow == 1
				&& $result->related()
				&& index($result->text_basic(),'may refer to') != -1
				&& split(' ', $query) > 1) {
			my @related = $result->related();
			my @relatedDocs = $self->retrieveDocuments(0, \@related);
			push @documents, @relatedDocs;
		}

		# Clean document and add it to list of docs
		my $text = $result->fulltext_basic();
		$text = $self->cleanText($text);
		push @documents, $text;

		# Learn a language model with the summary text
		$self->grammar_gram()->addText(
			$self->cleanText($result->text_basic())
		);
	}
	return @documents;
}

# Given a document and a phrase, returns a list of sentences (passages) that
# contain 65 % of the words in the phrase.
sub retrievePassages {
	my ($text, $phrase, @terms) = @_;

	# Create bag of words from phrase
	my $phrase_unigram = NGram->new( n => 1 );
	$phrase_unigram->addText($phrase);
	my @bag = keys %{$phrase_unigram->hash()->{''}};
	@bag = grep { $_ ne $NGram::COUNT && $_ ne '?' && $_ !~ /$COMMON/} @bag;

	# Threshold for percentage of words needed in sentence.
	my $threshold = 0.65;

	# Add passages if they contain 65% of the bag of words
	my @passages = ();
	my @sentences = split '(\.|\!|\?|\;)', $text;
	foreach my $sentence (@sentences) {
		$sentence = trim($sentence);

		# Use unigram to create hash of words in sentence
		my $unigram = NGram->new( n => 1 );
		$unigram->addText($sentence);

		# Count the words that the sentence and bag of words share
		my $count = 0;
		foreach my $word (@bag) {
			($count += 1) if (exists $unigram->hash()->{''}->{$word});
		}

		# Count the words that the sentence and terms share
		my $term_count = 0;
		foreach my $term (@terms) {
			($term_count += 1) if (exists $unigram->hash()->{''}->{$term});
		}

		# Add passage if it has 65% of the words or 10% of the terms
		if ($count / @bag > $threshold) {
			push @passages, $sentence;
		} elsif ($term_count / @terms > 0.10) {
			push @passages, $sentence;
		}
	}
	return removeDuplicates(@passages);
}


################################################################################
# 3) Code for Answer Processing ################################################
################################################################################

# Given a passage and a template, attempts to fill the '_' placeholder in the
# template and return the filled template. Otherwise returns undef;
sub extractAnswer {
	my ($passage, $template) = @_;
	# Ensure passage ends in a stop-word
	$passage .= '.';

	# print "Passage: $passage\nTemplate: $template\n";

	no warnings 'uninitialized';
	my $left = (split '_', $template)[0];
	my $right = (split '_', $template)[1];

	# Don't let string become 'undef'
	$left = defined $left ? trim($left) : '';
	$right = defined $right ? trim($right) : '';

	# print "$left, $right\n\n";

	if ($passage =~ /(\Q$left\E .*? \Q$right\E .*? (\. | \!))/xmsi) {
		return trim($1);
	}
	return undef;
}

# Given a passage and a template, attempts to match the template against the
# passage by ignoring the '_' only checking if the passage has all of the words
# in the same order given in the template. Returns the passage bounded by the
# left edge of the template and an end-of-sentence marker. Otherwise returns
# undef when there is no match.
sub extractLooseAnswer {
	my ($passage, $template) = @_;
	# Ensure passage ends in a stop-word
	$passage .= '.';

	no warnings 'uninitialized';
	my $left = (split '_', $template)[0];
	my $right = (split '_', $template)[1];

	# Don't let string become 'undef'
	$left = defined $left ? trim($left) : '';
	$right = defined $right ? trim($right) : '';

	# Swap whitespace for '.*?' literal
	$left =~ s/ /\.\*\?/g;
	$right =~ s/ /\.\*\?/g;

	if ($passage =~ /($left \b .*? \b $right \b .*? (\. | \!))/xmsi) {
		return trim($1);
	}
	return undef;
}



# Given a question and an answer, attempts to replace the subject of the answer
# with the subject of the question. If the answer has a form of 'to be' inside
# it, remove text before that word and insert the proper subject given from the
# question. Returns either the original answer or a corrected version.
sub rewordAnswer {
	my ($self, $question, $answer) = @_;
	unless ($answer) { return; }

	my $question_word = lc($self->getQuestionWordOrUndef($question));
	my $structs = $structures->{$self->language()};


	foreach my $be (@{$structs->{be}}) {
		# Regex for matching proper nouns
		my $q1_regex = qr {
			^ .*? \s $be \s (([A-Z]\w+\s?)+) .* \? $
		}xms; # must be case insensitive

		# Fallback regex
		my $q2_regex = qr {
			^ .*? \s $be \s (.*) \? $
		}xms;

		# Try to change the subject
		if ($question =~ $q1_regex || $question =~ $q2_regex) {
			my @answ_array = split $be, $answer;
			if (defined $answ_array[1]) {
				$answer = trim($1).' '.$be.' '.trim($answ_array[1]);
			}
		}
	}
	# Uppercase first letter
	$answer =~ s/^([a-z])/\u$1/;

	# Remove spaces before commas
	$answer =~ s/ ,/,/g;

	return $answer;
}

# Removes duplicates from an array while maintaining order
# Adapted from:
# http://stackoverflow.com/questions/7651/how-do-i-remove-duplicate-items-from-an-array-in-perl
sub removeDuplicates {
    my %seen = ();
    my @r = ();
    foreach my $a (@_) {
        unless ($seen{$a}) {
            push @r, $a;
            $seen{$a} = 1;
        }
    }
    return @r;
}

sub getRankings {
	my ($self, $question, @answers) = @_;
	my $structs = $structures->{$self->language()};
	my $question_word = lc($self->getQuestionWordOrUndef($question));

	# Create a bag of words from the question
	my $q_unigram = NGram->new( n => 1 );
	$q_unigram->addText($question);
	my @bag = keys %{$q_unigram->hash()->{''}};
	@bag = grep { $_ ne $NGram::COUNT && $_ ne '?' && $_ !~ /$COMMON/} @bag;

	my %rankings = ();
	my $index = 0;
	foreach my $answer (@answers) {
		next unless $answer;

		# Consider the likelihood of the answer occurring in English
		my $language = $self->grammar_gram()->getProbability($answer);

		# Consider the order in the answers
		my $order = 1 - ($index / @answers);

		# Consider the percentage of words contained in the answer that are
		# also in the question
		my $unigram = NGram->new( n => 1 );
		$unigram->addText($answer);
		my $count = 0;
		foreach my $word (@bag) {
			($count += 1) if (exists $unigram->hash()->{''}->{$word});
		}
		my $common = $count / @bag;

		# Create the ranking
		my $ranking = $language + $order + $common;

		# Penalize ranking for not ending in period
		($ranking -= 1) if ($answer !~ /\.$/i);

		# Penalize ranking for not adhering to question type
		if ($structs->{when} eq $question_word) {
			# favor months and years
			my $months = qr {
				^.*?(January | February |March |
				April |May | June | July |
				August | September | October |
				November | December).*?$
			}xmsi;
			if ($answer !~ /$months/ && $answer !~ /^.*?\d\d\d\d.*?$/) {
				$ranking -= 1;
			}
		}

		$rankings{$answer} = $ranking * (100 / 3);
		$index += 1;
	}
	return %rankings;
}

# Given a question and a list of anwers, attempts to pick an answer and return
# it. The first answer is chosen unless 'when' is the question word. In that
# case, the first answer that contains a month or year is returned. Returns
# undef if no answer is chosen.
sub pickAnswer {
	my ($self, $question, @answers) = @_;

	my %rankings = $self->getRankings($question, @answers);

	my @sorted = (sort {$rankings{$b} <=> $rankings{$a}} keys %rankings);
	my $answer = $sorted[0];

	# Log each answer
	foreach my $key (@sorted) {
		$self->log('Score: '.$rankings{$key}." Candidate: $key");
	}

	# Return an answer if confident
	if ($answer && $rankings{$answer} > 10) {
		return $answer;
	}

	# No answer chosen, return undef
	return undef;
}

# Given a question and a list of answers, attempts to ngram tile the second
# answer onto the first, and attempts to loosely add words from the second
# answer into the first answer. Returns these two attempts in a list.
sub tileAnswers {
	my ($self, $question, @answers) = @_;

	# Get first and second highest ranked answers
	my %rankings = $self->getRankings($question, @answers);
	my @sorted = (sort {$rankings{$b} <=> $rankings{$a}} keys %rankings);

	return unless exists $sorted[0] && exists $sorted[1];
	my $one = $sorted[0] =~ s/\./ /r;
	my $two = $sorted[1];
	$one = trim($one);
	$two = trim($two);

	# Create bigrams and trigrams
	my $bigram = NGram->new(n => 2);
	my $trigram = NGram->new(n => 3);
	$bigram->addText($one);
	$trigram->addText($one);

	# Tile ngrams onto the first answer
	my @words = split ' ', $one;
	for (my $i = $#words; exists $words[$i]; $i += 1) {
		my $word = $words[$i];
		if (exists $bigram->hash()->{$word}) {
			my $match = (sort {
				$bigram->hash()->{$word}->{$a}
				<=> $bigram->hash()->{$word}->{$b}
			} keys %{$bigram->hash()->{$word}})[0];
			push @words, $bigram->hash()->{$word}->{$match};
		}
	}


	# Add the tiled ngams to a list
	my @sentences = ();
	unshift @sentences, (join ' ', @words);

	# Attempt to fit the first answer into the second answer
	my $template = defined $one ? trim($one) : '';
	$one =~ s/ /\.\*\?/g;
	if ($two =~ /($template (\. | \!))/xmsi) {
		unshift @sentences, trim($1);
	}

	# push @sentences, $fitted;
	return @sentences;
}

################################################################################
# Return 1 since this is a module ##############################################
################################################################################
1;
