#!/usr/bin/env perl
#
# Tim Schoenheider
# 12-12-2013
# 
package WordNetSynset;
use strict;
use warnings;
use Readonly;
use LWP::Simple;

Readonly my $syn_regex => qr {
	<a \s href=\" .*? s= [a-z]+ \">
	(.*?)
	</a>
}xsmi;

Readonly my $wordnetweb => 'http://wordnetweb.princeton.edu/perl/webwn?s=';

# Uses the prineton.edu web interface to worknet in order to find synonyms
# for a given word, and return them in a list.
sub getSynset {
	my $word = $_[0];
	my $url = $wordnetweb.$word;
	my $html = get($url);
	return unless $html;

	my @synset = ();

	while ($html =~ /$syn_regex/g) {
		my $synonym = $1;
		push @synset, $synonym;
	}
	return @synset;
}

1;